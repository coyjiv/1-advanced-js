"use strict";

class Employee{
    constructor(name, age, salary){
        this.name = name;
        this.age = age;
        this.salary = salary;
    }
    set name(value){
        return this._name=value;
    }
    get name(){
        return this._name;
    }
    set age(value){
        return this._age=value;
    }
    get age(){
        return this._age;
    }
    set salary(value){
        return this._salary=value;
    }
    get salary(){
        return this._salary;
    }
}
class Programmer extends Employee{
    constructor(name, age, salary, lang){
        super(name, age, salary);
        this.lang = lang;
    }
    set salary(value){
        return this._salary=value;
    }
    get salary(){
        return this._salary*3;
    }
}

const daniel = new Programmer("Daniel Narodistky", 23, 20000, "C++, Python");
const adjay = new Programmer("Adjay Giri", 29, 15000, "Java Script");
const stanislav = new Programmer("Stanislav Bledniy", 19, 14000, "Go"); 

console.log(daniel, adjay, stanislav);